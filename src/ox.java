
import java.util.Scanner;

public class ox {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        
        // create table
        char[][] table = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }
        }

        System.out.println("Welcome to OX Game");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
        // game finish check
        boolean finish = false;
        // game turn check
        int count = 0;
        // start game
        while (finish != true) {
            //Turn O
            System.out.println("Turn O");
            System.out.println("Please input row, col:  ");
            table[kb.nextInt() - 1][kb.nextInt() - 1] = 'O';

            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(table[i][j] + " ");
                }
                System.out.println("");
            }
            //Check for O win
            if((table[0][0] == 'O' && table[0][1] == 'O' && table[0][2] == 'O')){
                System.out.println(">>>O win<<<");
                finish = true;
                break;
            }
            else if((table[1][0] == 'O' && table[1][1] == 'O' && table[1][2] == 'O')){
                System.out.println(">>>O win<<<");
                finish = true;
                break;
            }
            else if((table[2][0] == 'O' && table[2][1] == 'O' && table[2][2] == 'O')){
                System.out.println(">>>O win<<<");
                finish = true;
                break;
            }
            else if((table[0][0] == 'O' && table[1][0] == 'O' && table[2][0] == 'O')){
                System.out.println(">>>O win<<<");
                finish = true;
                break;
            }
            else if((table[0][1] == 'O' && table[1][1] == 'O' && table[2][1] == 'O')){
                System.out.println(">>>O win<<<");
                finish = true;
                break;
            }
            else if((table[0][2] == 'O' && table[1][2] == 'O' && table[2][2] == 'O')){
                System.out.println(">>>O win<<<");
                finish = true;
                break;
            }
            else if((table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O')){
                System.out.println(">>>O win<<<");
                finish = true;
                break;
            }
            else if((table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O')){
                System.out.println(">>>O win<<<");
                finish = true;
                break;
            }
            else{
                count +=1;
            }
            //Check Draw
            if(count <= 9){
                System.out.println(">>> Draw <<<");
                finish = true;
                break;
            }
            
            //Turn X
            System.out.println("Turn X");
            System.out.println("Please input row, col:  ");
            table[kb.nextInt() - 1][kb.nextInt() - 1] = 'X';

            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(table[i][j] + " ");
                }
                System.out.println("");
                
            }
            

        }

    }

}
